package Week03;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean done = false;
        do {
            // open scanner for input
            Scanner in = new Scanner(System.in);
            System.out.println("Please enter two numbers to be divided.");
            System.out.print("Input the first number: ");
            // takes input and puts it into a variable
            int x = in.nextInt();
            System.out.print("Input the second number: ");
            int y = in.nextInt();

            try {

            // show answer as output
                int answer = (x / y);
                System.out.println("Answer: " + answer);
                done = true;
            }

            catch (ArithmeticException why) {
                System.out.println ("Exception caught: Divisor is zero, try again.");
            }
        }while (!done);
    }
}


